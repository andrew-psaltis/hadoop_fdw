/*-------------------------------------------------------------------------
 *
 * flume_client.h
 *                A Socket Client for Flume
 *
 * Portions Copyright (c) 2012-2013, BigSQL Development Group
 *
 * IDENTIFICATION
 *                hadoop_fdw/src/flume_client.h
 *
 *-------------------------------------------------------------------------
 */
#ifndef FLUME_CLIENT_H
#define FLUME_CLIENT_H

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

typedef enum
{
	FLUME_CONNECTION_OK,
	FLUME_CONNECTION_BAD,
} FL_ConnStatusType;

typedef struct
{
	struct sockaddr_storage addr;
	ACCEPT_TYPE_ARG3 salen;
} FL_SockAddr;

typedef struct flume_conn
{
	char		   *host;
	char		   *port;

	struct addrinfo *addrlist;
	struct addrinfo *addr_cur;

	char		   *inBuffer;
	int				inBufSize;
	int				inStart;

	char		   *outBuffer;
	int				outBufSize;
	int				outCount;
	int				outMsgEnd;

	char		   *errorMessage;
	int				errorMsgSize;

	int				sock;
	FL_SockAddr	 	addr;
	FL_ConnStatusType  status;
} flume_conn;

typedef struct flume_conn FlumeConnection;

extern FlumeConnection *flumeConnect(char* address, char* port);
extern void flumeCloseConnection(FlumeConnection *conn);
extern int flumePutMessage(FlumeConnection *conn, char *msg);
#endif   /* FLUME_CLIENT_H */
